#!/bin/bash
#curl -X POST -u admin:token "http://host:ip/job/test-obj/myconfig.xml" -d "@myconfig.xml"

# jenkins ip:port or domain
ip="http://ip:port"
# jenkins user:token
usertoken="admin:token"

# the command with get config from created job
if [ "$1" == "create-config" ]; then
    curl -u $usertoken -X GET $ip/job/$2/config.xml -o myconfig.xml

elif [ "$1" == "create-job" ]; then
    # create job
    curl -s -XPOST $ip/createItem?name=$2 -u $usertoken --data-binary @myconfig.xml -H "Content-Type:text/xml"

elif [ "$1" == "reconfig-job" ]; then
    # reconfig job
    curl -u $usertoken -X POST $ip/job/$2/config.xml -H 'Content-Type: application/xml' --data-binary "@myconfig.xml"

elif [ "$1" == "delete-job" ]; then
    #delete job
    curl -XPOST $ip/job/$2/doDelete --user $usertoken

else
    echo """
  jenkins job processes with jenkins API. each command reads or writes myconfig.xml

    create-config   <job-name>   :  create myconfig.xml with a jenkins job
    create-job      <job-name>   :  create job, using myconfig.xml for job config
    reconfig-job    <job-name>   :  reconfig job, using myconfig.xml for job config
    delete-job      <job-name>   :  delete job

"""
fi
