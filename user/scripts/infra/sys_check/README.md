# Linux System check scripts

## Usable tools(command)

### Disk
 - lsof
 - vmstat

### CPU
 - vmstat

### IO
 - vmstat
 - sar
 - iostat

### Network
 - tcpdump
 - netstat
 - iptraf
 - nethogs
 - iftop
 - Monitorix
 - arpwatch

### Process
 - vmstat
 - top
 - htop
 - iotop
 - Monitorix

### User Activity
 - psacct
 - acct


