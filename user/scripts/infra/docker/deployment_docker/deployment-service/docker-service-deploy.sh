#!/bin/bash
source $CONF_DIR

set -e # EXIT ON FIRST ERROR


function zulipSend {
  echo "dummy_zulipsend"
  curl  -s -X POST ${ZULIP_MESSAGES_API} \
        ${SEND_TO_STREAM_OR_PRIVATE} \
        -u $ZULIP_BOT_EMAIL_ADDRESS:$ZULIP_BOT_API_KEY \
        --data-urlencode "subject=${GIT_NAME} / ${MY_GIT_BRANCH}" \
        --data-urlencode "content=$1"
}

# Absolute path to this script, e.g. /home/user/bin/foo.sh
ME_REL=${0}
ME_ABS=$(readlink -f "${ME_REL}")
MYFILENAME=$(basename "${ME_ABS}")
MYFULLPATH=$(dirname "${ME_ABS}")
MYDIRNAME=$(basename "${MYFULLPATH}")

CONF_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )/config/docker-service.conf"

for i in "$@"
do
    case $i in
        --git-url=*)
            MY_GIT_URL=$1
        ;;

        --branch-name=*) ## Not using this variable point internal network docker services
            MY_GIT_BRANCH=$(basename "$2")
        ;;

        --build-number=*) ## Not using this variable point internal network docker services
            MY_BUILD_NUMBER=$3
        ;;

        --jenkins)
            echo "Gettting all params from ENV values."
            MY_GIT_URL=${GIT_URL}
            MY_GIT_BRANCH=$(basename "${GIT_BRANCH}")
            MY_BUILD_NUMBER=${BUILD_NUMBER}
            echo "BUILD_NUMBER=$MY_BUILD_NUMBER, GIT_URL=$MY_GIT_URL, GIT_BRANCH=$MY_GIT_BRANCH"
        ;;
        -h|--help)
            echo """
Parameters docker-service-deploy.sh: 

    --git-url=         :    git url
    --branch-name=     :    git repo branch name 
    --build-number=    :    docker image build number 
    --jenkins          :    if work in jenkins, write the parameter
"""
            exit
;;
    esac
done


echo "
###########################################
### STARTING DOCKER SERVICE AUTO DEPLOY ###
###########################################
MYFILENAME : ${MYFILENAME}
MYDIRNAME  : ${MYDIRNAME}
MYFULLPATH : ${MYFULLPATH}
ME_ABS     : ${ME_ABS}
###########################################
"


if ${FLAG_DEBUG}; then
echo "
###########################################
###  DUMPING ALL ENVS & ENABLING DEBUG  ###
###########################################
$(env | sort)
###########################################
"
# export | sort
#set -x # ENABLE DEBUG
SEND_TO_STREAM_OR_PRIVATE="--data-urlencode type=private --data-urlencode to=${ZULIP_DEBUGGER_EMAIL}"   # For testing & debugging - use private messages
fi


PRJ_TYPE_URL=$(dirname "${MY_GIT_URL}")
PRJ_TYPE=$(basename "${PRJ_TYPE_URL}")
GIT_NAME=$(basename "${MY_GIT_URL}")
PRJ_NAME=$(echo "${GIT_NAME}" | sed -e 's|.git||g')
FLAG_BUILD_TEST=false
FLAG_BUILD_PROD=false

case ${MY_GIT_BRANCH} in
    "draft*"|"master*")
        echo "INFO: This is a draft branch. There will be no build processed. Exitting..."
        exit
    ;;
    "test*")
        FLAG_BUILD_TEST=true
        echo "### TEST BUILD ###"
    ;;
    "prod*")
        FLAG_BUILD_PROD=true
        echo "### PRODUCTION BUILD ###"
    ;;
esac

case ${MY_GIT_BRANCH} in
    "test"|"prod")            # for the main branches (dont add the extra branch tag)
        ADD_TAG=""
    ;;
    "*")
        ADD_TAG="_${MY_GIT_BRANCH}"
        PRJ_NAME="${PRJ_NAME}${ADD_TAG}"
        echo "### Branch tag added to PRJ_NAME: ${PRJ_NAME} ###"
    ;;
esac

PRJ_FULLNAME="${PRJ_TYPE}_${PRJ_NAME}"

if ${FLAG_BUILD_TEST}; then
    VERSION="${MY_GIT_BRANCH}"
    MY_BUILD_NUMBER=$(git describe --always)   # use short commit id as build_number # sample: 'hub.MYDOMAIN.app/MYAPP/backend/test/test-api:test.v2.5.5-1019-g12f988f'
    TARGET_SERVERS=${TEST_SERVERS}
    SERVICE_NAME="${SERVICE_NAME_TEST_INIT}${SERVICE_NAME_INIT}${PRJ_FULLNAME}" # SAMPLE: test_MYAPP-backend_api
else
    TARGET_SERVERS=${PROD_SERVERS}
    SERVICE_NAME="${SERVICE_NAME_INIT}${PRJ_FULLNAME}" # SAMPLE: MYAPP-backend_api
fi

if ${FLAG_SINGLESERVER}; then
    TARGET_SERVERS=$(hostname)
fi

CUSTOM_LOGIC_FILE="${ME_ABS}.custom_logic"
if test -f ${CUSTOM_LOGIC_FILE}; then            # You may ovverride the above variables by creating this ".custom_logic" file in $CWD
    source "${CUSTOM_LOGIC_FILE}"
fi

BUILD_CONSOLE_URL="${BUILD_URL}consoleFull"
IMAGE_ID_VER=${SERVICE_NAME}:${VERSION}.${MY_BUILD_NUMBER}
IMAGE_ID_LTS=${SERVICE_NAME}:latest

echo "================= STARTING ===================
MY_GIT_URL           : ${MY_GIT_URL}
MY_GIT_BRANCH        : ${MY_GIT_BRANCH}
MY_BUILD_NUMBER      : ${MY_BUILD_NUMBER}
FLAG_BUILD_TEST      : ${FLAG_BUILD_TEST}
FLAG_SINGLESERVER    : ${FLAG_SINGLESERVER}
FLAG_BUILD_PROD      : ${FLAG_BUILD_PROD}
PRJ_TYPE             : ${PRJ_TYPE}
PRJ_NAME             : ${PRJ_NAME}
SERVICE_NAME         : ${SERVICE_NAME}
BUILD_URL            : ${BUILD_URL}
"

MESSAGE=':white_flag: Building ['${PRJ_NAME}']('${MY_GIT_URL}'):['${MY_GIT_BRANCH}']('${MY_GIT_URL}'/-/tree/'${MY_GIT_BRANCH}') => [ '${TARGET_SERVERS}' ] (See [log]('${BUILD_CONSOLE_URL}'))'
zulipSend "${MESSAGE}"

DOCKERBUILD_OPTIONS=""          # DOCKERBUILD_OPTIONS="--no-cache"     # !!! Use only when needed, may fix some docker-build problems, bu slows down a lot. 

if BUILD_RESULT=$(docker build ${DOCKERBUILD_OPTIONS} --tag ${IMAGE_ID_VER} .); then
    zulipSend ":working_on_it: Build succesfull: ${IMAGE_URL}"
else
    BUILD_RESULT_TAIL=$(echo ${BUILD_RESULT} | tail -n1)
    MESSAGE=':exclamation: Build failed. => See erros in [jenkins log]('${BUILD_CONSOLE_URL}') <=
```
'${BUILD_RESULT_TAIL}'
```'
    zulipSend "${MESSAGE}"
    exit 101
fi
docker image tag ${IMAGE_ID_VER} ${IMAGE_ID_LTS}

if ${FLAG_SINGLESERVER}; then
    echo "*** STARING DEPLOYEMENT FOR SINGLE SERVER MODE ***"
    IMAGE_URL="local_${IMAGE_ID_VER}"
    docker image tag ${IMAGE_ID_VER} ${IMAGE_URL}
    IMAGE_URL_LATEST=${HUB_URL}/${PRJ_TYPE}/${PRJ_NAME}:latest
    RUNCMD="/root/bin/docker-service-create-update.sh --local ${SERVICE_NAME} ${IMAGE_URL}"
else
    echo "*** STARING DEPLOYEMENT FOR MULTI SERVER MODE ***"
    echo ">>> TARGET_SERVERS => ${TARGET_SERVERS}" 
    IMAGE_URL=${HUB_URL}/${PRJ_TYPE}/${IMAGE_ID_VER}
    IMAGE_URL_LATEST=${HUB_URL}/${PRJ_TYPE}/${PRJ_NAME}:latest
    echo ">>> IMAGE_URL => ${IMAGE_URL}"
    # docker tag ${IMAGE_ID_VER} ${IMAGE_ID_LTS} # ${IMAGE_URL}
    # docker tag ${IMAGE_ID_VER} ${IMAGE_ID_LTS} # ${IMAGE_URL_LATEST}
    docker image push ${IMAGE_URL}
    docker image push ${IMAGE_URL_LATEST}
    zulipSend ":check: Image published as: ${IMAGE_URL}"

    # use mssh (multi ssh script) to update on all docker-swarm servers########
    RUNCMD="/root/bin/mssh" "${TARGET_SERVERS}" "/root/bin/docker-service-create-update.sh ${SERVICE_NAME} ${IMAGE_URL}"
    ###########################################################################
fi
DEPLOY_RESULT=$( $RUNCMD ) # run RUNCMD
DEPLOY_EXITCODE=$?

if [ $DEPLOY_EXITCODE -ne 0 ]; then
    echo "!!! ERROR !!!"
    DEPLOY_RESULT_TAIL=$(echo ${DEPLOY_RESULT} | tail -n1)
    MESSAGE=':no_entry: Deploy failed:
```
'${DEPLOY_RESULT_TAIL}'
```'
    zulipSend "${MESSAGE}"
    exit $DEPLOY_EXITCODE
else
    zulipSend ":check_mark: Service deployed: ${SERVICE_NAME}  => [ ${TARGET_SERVERS} ]"
fi

