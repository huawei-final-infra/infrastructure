#!/bin/bash

ZULIP_STREAM="$2"
subject="$1" # service_name
SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )"

function zulipSend {
  echo "dummy_zulipsend"
  curl  -s -X POST ${ZULIP_MESSAGES_API} \
        ${SEND_TO_STREAM_OR_PRIVATE} \
        -u $ZULIP_BOT_EMAIL_ADDRESS:$ZULIP_BOT_API_KEY \
        --data-urlencode "subject=$subject" \
        --data-urlencode "content=$1"
}


source $SCRIPT_DIR/zulip-sender.conf

zulipSend "$3"
