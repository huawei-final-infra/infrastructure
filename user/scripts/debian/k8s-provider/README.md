# K8s provider scripts

this script for install k8s and prepares the machine for k8s.

## Using

 - k8s-install.sh : for all machines install k8s master and node(control plane and worker)
 - master.sh : shoul run just control plane machine 

### for Worker

should run 'kubeadm join ...' in the worker. can read control plane machine ~/token-k8s.txt

## firewall configration

### Control plane allow ports

 - 6443/tcp : api-server
 - 2379-2380 /tcp : etcd server
 - 10250/tcp : kubelet api
 - 10259/tcp : kube-scheduler
 - 10257/tcp : kube-controller-manager

### Worker allow ports

 - 10250/tcp : kubelet api
 - 30000-3267 /tcp : nodeport services

### calico master and node ports

 - 179/tcp
 - 4789/udp
 - 5473/tcp
 - 51820
 - 51821
