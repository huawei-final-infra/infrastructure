# Terraform Notes

## Install

 - https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/docker-get-started

## Using

create main.tf file and write with iac of terraform language.

```bash

terraform init
terraform apply 

# if you want remove this app.

terraform destroy
```
