# Infrastructure Project

---
 [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)


## Description

with the project can management of linux system easly. inside have scripts,config files and yaml files for orchestration(docker swarm,k8s).

### Dependencies for all files

#### for scripts

 - Bash
 - curl
 - rync

#### Other

 - containerd
 - Docker
 - k8s
 - ssh

## Using

run script or yaml files and move config files. okay infra is ready. this all files for easy management. 

### Files

 - command : usable commands for infra engineering. 
 - config : some configs for system,network,service or kernel.
   - etc : linux system etc files configs.
   - kernel-config : config for linux(kernel).This files is linux/.config files 
 - infra-yaml : yaml files to create a service.
   - docker 
   - k8s 
 - user : user home directory files. (like scripts, .ssh files)
   - scripts : Usable scripts
     - debian : debian system usable scripts
     - gentoo : gentoo system usable scripts
     - infra : orchestrations,services and some usable system management scripts.
     - request_api : some usable api request scripts.
   - .ssh : ssh files and a small script.
 - iac : iac files for terraform and ansible
   - ansible : some infra iac.
   - terraform : some infra iac
 - INSTALL : install some file on Linux system. 
